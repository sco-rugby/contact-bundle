<?php

namespace ScoRugby\ContactBundle;

use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

/**
 * Description of ScoRugbyContactBundle
 *
 * @author Antoine BOUET
 */
class ScoRugbyContactBundle extends AbstractBundle {
    
}
