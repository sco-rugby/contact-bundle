<?php

namespace ScoRugby\ContactBundle\Entity;

USE ScoRugby\CoreBundle\Entity\EntityInterface;
use ScoRugby\ContactBundle\Model\TypeMoyenContact;

class ContactEmail implements EntityInterface {

    protected ?int $id = null;
    protected ?Contact $contact = null;
    protected ?string $email = null;
    protected TypeMoyenContact $type;
    protected bool $prefere = false;

    public function __construct() {
        $this->type = new TypeMoyenContact();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self {
        $this->contact = $contact;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getType(): TypeMoyenContact {
        return $this->type;
    }
}
