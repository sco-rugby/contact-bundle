<?php

namespace ScoRugby\ContactBundle\Entity;

use ScoRugby\CoreBundle\Exception\InvalidParameterException;
use Symfony\Component\Intl\Countries;

class Adresse implements AdresseInterface {

    private ?string $adresse = null;
    private ?string $complement = null;
    private ?string $codePostal = null;
    private ?string $ville = null;
    private ?string $pays = null;

    public function getAdresse(): ?string {
        return $this->adresse;
    }

    public function setAdresse(?string $adresse): self {
        $this->adresse = $adresse;
        return $this;
    }

    public function getComplement(): ?string {
        return $this->complement;
    }

    public function setComplement(?string $complement): self {
        $this->complement = $complement;
        return $this;
    }

    public function getCodePostal(): ?string {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self {
        $this->codePostal = $codePostal;
        return $this;
    }

    public function getVille(): ?string {
        return $this->ville;
    }

    public function setVille(?string $ville): self {
        $this->ville = $ville;
        return $this;
    }

    public function getPays(): ?string {
        return $this->pays;
    }

    public function setPays(?string $pays): self {
        $pays = strtoupper($pays);
        if (!Countries::exists($pays)) {
            throw new InvalidParameterException(sprintf('Le code %s n\'est pas un code pays valide (ISO 3166-1 alpha-2)', $pays));
        }
        $this->pays = $pays;
        return $this;
    }
}
