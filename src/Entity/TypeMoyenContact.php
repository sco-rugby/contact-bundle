<?php

namespace ScoRugby\ContactBundle\Entity;

/**
 * Description of Type
 *
 * @author Antoine BOUET
 */
class TypeMoyenContact {

    protected ?string $id = null;
    protected ?string $libelle = null;
    protected bool $prefere = false;

    public function getId(): ?string {
        return $this->id;
    }

    public function setId(string $id): self {
        $this->id = $id;
        return $this;
    }

    public function getLibelle(): ?string {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self {
        $this->libelle = $libelle;
        return $this;
    }

    public function isPrefere(): ?bool {
        return $this->prefere;
    }

    public function setPrefere(bool $prefere = true): self {
        $this->prefere = $prefere;

        return $this;
    }
}
