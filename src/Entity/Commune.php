<?php

namespace ScoRugby\ContactBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ScoRugby\CoreBundle\Entity\EntityInterface;

final class Commune implements EntityInterface, \Stringable {

    private ?string $id = null;
    private ?string $type = null;
    private ?self $parent = null;
    private ?string $canonizedNom = null;
    private ?string $nom = null;
    private ?string $latitude = null;
    private ?string $longitude = null;
    private ?string $codeINSEE = null;
    private ?bool $regroupement = false;
    private Collection $communesAssociees;
    private Collection $codePostaux;

    public function __construct() {
        $this->communesAssociees = new ArrayCollection();
        $this->codePostaux = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function setId(int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getType(): ?string {
        return $this->type;
    }

    public function setType(string $type): self {
        $this->type = $type;

        return $this;
    }

    public function getNom(): ?string {
        return $this->nom;
    }

    public function setNom(string $nom): self {
        $this->nom = $nom;
        return $this;
    }

    public function getCanonizedNom(): ?string {
        return $this->canonizedNom;
    }

    public function setCanonizedNom(string $nom): self {
        $this->canonizedNom = $nom;
        return $this;
    }

    public function getLatitude(): ?string {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self {
        $this->longitude = $longitude;

        return $this;
    }

    public function getCodeINSEE(): ?string {
        return $this->codeINSEE;
    }

    public function setCodeINSEE(string $codeINSEE): self {
        $this->codeINSEE = $codeINSEE;

        return $this;
    }

    public function isRegroupement(): ?bool {
        return $this->regroupement;
    }

    public function setRegroupement(bool $regroupement): self {
        $this->regroupement = $regroupement;

        return $this;
    }

    public function getParent(): ?self {
        return $this->parent;
    }

    public function setParent(?self $parent): self {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getCommunesAssociees(): Collection {
        return $this->communesAssociees;
    }

    public function addCommuneAssociee(self $commune): self {
        if (!$this->communesAssociees->contains($commune)) {
            $this->communesAssociees->add($commune);
            $commune->setParent($this);
        }

        return $this;
    }

    public function removeCommunesAssociee(self $commune): self {
        if ($this->communesAssociees->removeElement($commune)) {
            // set the owning side to null (unless already changed)
            if ($commune->getParent() === $this) {
                $commune->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, CodePostal>
     */
    public function getCodePostaux(): Collection {
        return $this->codePostaux;
    }

    public function addCodePostaux(CodePostal $codePostaux): self {
        if (!$this->codePostaux->contains($codePostaux)) {
            $this->codePostaux->add($codePostaux);
            $codePostaux->setCommune($this);
        }

        return $this;
    }

    public function removeCodePostaux(CodePostal $codePostal): self {
        if ($this->codePostaux->removeElement($codePostal)) {
            // set the owning side to null (unless already changed)
            if ($codePostal->getCommune() === $this) {
                $codePostal->setCommune(null);
            }
        }

        return $this;
    }

    public function __toString(): string {
        return (string) $this->getNom();
    }
}
