<?php

namespace ScoRugby\ContactBundle\Entity;

use Doctrine\Common\Collections\Collection;
use ScoRugby\CoreBundle\Entity\EntityInterface;
use ScoRugby\ContactBundle\Model\Groupe as BaseGroupe;

class Groupe extends BaseGroupe implements EntityInterface {

    protected ?int $id = null;
    protected ?string $libelle = null;
    protected Collection $contacts;

    public function __construct() {
        parent::__construct();
        $this->contacts = new ArrayCollection();
    }
}
