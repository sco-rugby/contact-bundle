<?php

namespace ScoRugby\ContactBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ScoRugby\ContactBundle\Model\Adresse;
use ScoRugby\ContactBundle\Entity\Contact;
use ScoRugby\ContactBundle\Model\Organisation as BaseOrganisation;
use ScoRugby\CoreBundle\Entity\DateTimeBlameableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameable;
use ScoRugby\CoreBundle\Entity\EntityInterface;

Abstract class Organisation extends BaseOrganisation implements EntityInterface, DateTimeBlameableInterface {

    protected ?int $id = null;
    protected ?string $nom = null;
    protected Adresse $adresse;
    protected ?string $etat = 'A';
    protected Collection $contacts;
    private DateTimeBlameable $datetime;

    public function __construct() {
        parent::__construct();
        $this->contacts = new ArrayCollection();
        $this->datetime = new DateTimeBlameable();
    }

    public function getDateTime(): DateTimeBlameable {
        return $this->datetime;
    }

    /**
     * @return Collection<int, Contact>
     */
    public function getContacts(): Collection {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self {
        if (!$this->contacts->contains($contact)) {
            $this->contacts->add($contact);
            $contact->addOrganisation($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self {
        if ($this->contacts->removeElement($contact)) {
            $contact->removeOrganisation($this);
        }

        return $this;
    }
}
