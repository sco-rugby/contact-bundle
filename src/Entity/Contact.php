<?php

namespace ScoRugby\ContactBundle\Entity;

use ScoRugby\ContactBundle\Model\Contact as BaseContact;
use ScoRugby\ContactBundle\Model\Adresse;
use ScoRugby\ContactBundle\Entity\Commune;
use ScoRugby\ContactBundle\Entity\Organisation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ScoRugby\CoreBundle\Entity\EntityInterface;
use ScoRugby\CoreBundle\Model\ManagedResourceInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameableInterface;
use ScoRugby\CoreBundle\Entity\DateTimeBlameable;

class Contact extends BaseContact implements EntityInterface, DateTimeBlameableInterface, ManagedResourceInterface {

    protected Adresse $adresse;
    protected ?Commune $commune = null;
    protected Collection $groupes;
    protected Collection $organisations;
    protected Collection $telephones;
    protected Collection $emails;
    protected DateTimeBlameable $datetime;

    public function __construct() {
        $this->adresse = new Adresse();
        $this->groupes = new ArrayCollection();
        $this->organisations = new ArrayCollection();
        $this->telephones = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->datetime = new DateTimeBlameable();
    }

    public function getDateTime(): DateTimeBlameable {
        return $this->datetime;
    }

    public function getCommune(): ?Commune {
        return $this->commune;
    }

    public function setCommune(?Commune $commune): self {
        $this->commune = $commune;

        return $this;
    }

    /**
     * @return Collection<int, GroupeContact>
     */
    public function getGroupes(): Collection {
        return $this->groupes;
    }

    public function addGroupe(Groupe $groupe): self {
        if (!$this->groupes->contains($groupe)) {
            $this->groupes->add($groupe);
            $groupe->setContact($this);
        }

        return $this;
    }

    public function removeGroupe(Groupe $groupe): self {
        if ($this->groupes->removeElement($groupe)) {
            // set the owning side to null (unless already changed)
            if ($groupe->getContact() === $this) {
                $groupe > setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Organisation>
     */
    public function getOrganisations(): Collection {
        return $this->organisations;
    }

    public function addOrganisation(Organisation $organisation): self {
        if (!$this->organisations->contains($organisation)) {
            $this->organisations->add($organisation);
        }

        return $this;
    }

    public function removeOrganisation(Organisation $organisation): self {
        $this->organisations->removeElement($organisation);

        return $this;
    }

    /**
     * @return Collection<int, ContactTelephone>
     */
    public function getTelephones(): Collection {
        return $this->telephones;
    }

    public function addTelephone(ContactTelephone $telephone): self {
        if (!$this->telephones->contains($telephone)) {
            $this->telephones->add($telephone);
            $telephone->setContact($this);
        }

        return $this;
    }

    public function removeTelephone(ContactTelephone $telephone): self {
        if ($this->telephones->removeElement($telephone)) {
            // set the owning side to null (unless already changed)
            if ($telephone->getContact() === $this) {
                $telephone->setContact(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ContactTelephone>
     */
    public function getEmails(): Collection {
        return $this->emails;
    }

    public function addEmail(ContactEmail $email): self {
        if (!$this->emails->contains($email)) {
            $this->emails->add($email);
            $email->setContact($this);
        }

        return $this;
    }

    public function removeEmail(ContactEmail $email): self {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getContact() === $this) {
                $email->setContact(null);
            }
        }

        return $this;
    }
}
