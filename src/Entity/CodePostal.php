<?php

namespace ScoRugby\ContactBundle\Entity;

use ScoRugby\CoreBundle\Entity\EntityInterface;

final class CodePostal implements EntityInterface {

    private ?int $id = null;
    private ?string $codePostal = null;
    private ?Commune $commune = null;

    public function getId(): ?int {
        return $this->id;
    }

    public function getCodePostal(): ?string {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getCommune(): ?Commune {
        return $this->commune;
    }

    public function setCommune(?Commune $commune): self {
        $this->commune = $commune;

        return $this;
    }
}
