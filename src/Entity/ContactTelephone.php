<?php

namespace ScoRugby\ContactBundle\Entity;

use ScoRugby\CoreBundle\Exception\InvalidParameterException;
use Symfony\Component\Intl\Countries;
USE ScoRugby\CoreBundle\Entity\EntityInterface;

class ContactTelephone implements EntityInterface {

    protected ?int $id = null;

    #[ORM\JoinColumn(nullable: false)]
    protected ?Contact $contact = null;
    protected ?string $codePays = null;
    protected ?string $numero = null;
    protected TypeMoyenContact $type;

    public function __construct() {
        $this->type = new TypeMoyenContact();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getContact(): ?Contact {
        return $this->contact;
    }

    public function setContact(?Contact $contact): self {
        $this->contact = $contact;

        return $this;
    }

    public function getCodePays(): ?string {
        return $this->codePays;
    }

    public function setCodePays(string $pays): self {
        $pays = strtoupper($pays);
        if (!Countries::exists($pays)) {
            throw new InvalidParameterException(sprintf('Le code %s n\'est pas un code pays valide (ISO 3166-1 alpha-2)', $pays));
        }
        $this->codePays = $pays;

        return $this;
    }

    public function getNumero(): ?string {
        return $this->numero;
    }

    public function setNumero(string $numero): self {
        $this->numero = $numero;

        return $this;
    }

    public function getType(): TypeMoyenContact {
        return $this->type;
    }
}
