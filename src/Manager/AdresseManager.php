<?php

namespace ScoRugby\ContactBundle\Manager;

use ScoRugby\ContactBundle\Model\Adresse;
use ScoRugby\ContactBundle\Entity\Commune;
use ScoRugby\ContactBundle\Repository\CommuneRepository;

/**
 * Description of AdresseManager
 *
 * @author Antoine BOUET
 */
class AdresseManager {

    public function __construct(private CommuneRepository $repository, private EntityManagerInterface $em) {
        return;
    }

    public function findByVille(string $ville): ?Commune {
        return $this->repository->findByNom($ville);
    }

    static public function normalize(Adresse &$adresse): void {
        $normalizer = new AdresseMormalizer();
        $adresse = $normalizer->normalize($adresse, Adresse::class);
    }
}
